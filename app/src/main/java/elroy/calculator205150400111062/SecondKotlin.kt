package elroy.calculator205150400111062
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView

class SecondKotlin : AppCompatActivity(){
    private lateinit var tvResult: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        var tvResultFix = findViewById<TextView>(R.id.tvResultFix)
        var tvExpressionFix = findViewById<TextView>(R.id.tvExpressionFix)
        val intent = intent
        val exp = intent.getStringExtra("b")
        var nilai= intent.getStringExtra("a")
        tvExpressionFix.text = exp
        tvResultFix.text =nilai
    }
}